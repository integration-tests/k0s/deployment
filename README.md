#

## prepare

```bash
ansible-playbook -i inventories/k0s/inventory.yml playbooks/prepare.yml
```

## single controller

```bash
ansible-playbook -i inventories/single-controller/inventory.yml playbooks/k0s_controllers.yml
```

### test

```bash
export KUBECONFIG=/src/integrations/k0s-cluster/deployment/inventories/single-controller/artifacts/k0s-kubeconfig.yml

kubectl get nodes
NAME           STATUS   ROLES           AGE   VERSION
controller-1   Ready    control-plane   77s   v1.25.2+k0s

kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   118s

kubectl get endpoints
NAME         ENDPOINTS             AGE
kubernetes   192.168.125.11:6443   2m15s

kubectl get controlnodes
NAME           AGE
controller-1   2m22s

kubectl get namespaces
NAME              STATUS   AGE
default           Active   2m48s
k0s-autopilot     Active   2m46s
k0s-system        Active   2m37s
kube-node-lease   Active   2m50s
kube-public       Active   2m50s
kube-system       Active   2m50s
```


## multi controller with one worker

```bash
ansible-playbook -i inventories/multi-controllers-single-worker/inventory.yml playbooks/k0s.yml
```

### test

```bash

export KUBECONFIG=/src/integrations/k0s-cluster/deployment/inventories/multi-controllers-single-worker/artifacts/k0s-kubeconfig.yml

kubectl get nodes
NAME       STATUS   ROLES    AGE     VERSION
worker-1   Ready    <none>   5m16s   v1.25.2+k0s

kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   6m12s

kubectl get endpoints
NAME         ENDPOINTS             AGE
kubernetes   192.168.125.11:6443,192.168.125.12:6443,192.168.125.13:6443   41m

kubectl get controlnodes
NAME           AGE
controller-1   41m
controller-2   4m5s
controller-3   3m59s

kubectl get namespaces
NAME              STATUS   AGE
default           Active   6m48s
k0s-autopilot     Active   6m45s
k0s-system        Active   6m35s
kube-node-lease   Active   6m49s
kube-public       Active   6m50s
kube-system       Active   6m50s
openebs           Active   6m28s
```


## full cluster

```bash
ansible-playbook -i inventories/k0s-cluster/inventory.yml playbooks/k0s.yml
```

### test

```bash
export KUBECONFIG=/src/integrations/k0s-cluster/deployment/inventories/k0s-cluster/artifacts/k0s-kubeconfig.yml

kubectl get nodes
NAME       STATUS   ROLES    AGE     VERSION
worker-1   Ready    <none>   4m46s   v1.25.2+k0s
worker-2   Ready    <none>   111s    v1.25.2+k0s
worker-3   Ready    <none>   113s    v1.25.2+k0s

kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   5m55s

kubectl get endpoints
NAME         ENDPOINTS                                                     AGE
kubernetes   192.168.125.11:6443,192.168.125.12:6443,192.168.125.13:6443   6m8s

kubectl get controlnodes
NAME           AGE
controller-1   6m6s
controller-2   5m56s
controller-3   6m1s

kubectl get namespaces
NAME              STATUS   AGE
default           Active   6m34s
k0s-autopilot     Active   6m31s
k0s-system        Active   6m18s
kube-node-lease   Active   6m36s
kube-public       Active   6m36s
kube-system       Active   6m36s
openebs           Active   6m13s
```

## reset cluster

```bash
ansible-playbook -i inventories/k0s/inventory.yml playbooks/k0s_reset.yml
```

